/*
SQLyog Community v13.1.9 (64 bit)
MySQL - 8.0.33 : Database - school_bar
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`school_bar` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;

USE `school_bar`;

/*Table structure for table `city` */

DROP TABLE IF EXISTS `city`;

CREATE TABLE `city` (
  `cid` int NOT NULL,
  `city` varchar(50) NOT NULL,
  `pid` int DEFAULT NULL,
  PRIMARY KEY (`city`),
  KEY `fk_city_pid` (`pid`),
  CONSTRAINT `fk_city_pid` FOREIGN KEY (`pid`) REFERENCES `provincial` (`pid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

/*Data for the table `city` */

insert  into `city`(`cid`,`city`,`pid`) values 
(10,'七台河市',10),
(7,'万宁市',24),
(2,'三亚市',24),
(4,'三明市',14),
(12,'三门峡市',17),
(1,'上海市',3),
(11,'上饶市',15),
(8,'东方市',24),
(17,'东莞市',20),
(5,'东营市',16),
(5,'中卫市',30),
(18,'中山市',20),
(13,'临夏回族自治州',21),
(10,'临汾市',6),
(13,'临沂市',16),
(8,'临沧市',25),
(12,'临高县',24),
(6,'丹东市',8),
(11,'丽水市',12),
(6,'丽江市',25),
(9,'乌兰察布市',32),
(3,'乌海市',32),
(21,'乌苏市',31),
(1,'乌鲁木齐市',31),
(15,'乐东黎族自治县',24),
(10,'乐山市',22),
(4,'九江市',15),
(16,'云林县',7),
(21,'云浮市',20),
(6,'五家渠市',31),
(3,'五指山市',24),
(15,'亳州市',13),
(13,'仙桃市',18),
(7,'伊 春 市',10),
(18,'伊宁市',31),
(6,'佛山市',20),
(9,'佳木斯市',10),
(17,'保亭黎族苗族自治县',24),
(6,'保定市',5),
(4,'保山市',25),
(15,'信阳市',17),
(5,'儋州市',24),
(2,'克拉玛依市',31),
(14,'六安市',13),
(2,'六盘水市',23),
(1,'兰州市',21),
(11,'兴安盟',32),
(9,'内江市',22),
(21,'凉山彝族自治州',22),
(2,'包头市',32),
(1,'北京市',1),
(5,'北海市',28),
(3,'十堰市',18),
(1,'南京市',11),
(11,'南充市',22),
(1,'南宁市',28),
(7,'南平市',14),
(15,'南投县',7),
(1,'南昌市',15),
(6,'南通市',11),
(13,'南阳市',17),
(17,'博乐市',31),
(2,'厦门市',14),
(4,'双鸭山市',10),
(22,'台东县',7),
(13,'台中县',7),
(4,'台中市',7),
(8,'台北县',7),
(1,'台北市',7),
(18,'台南县',7),
(5,'台南市',7),
(10,'台州市',12),
(1,'合肥市',13),
(8,'吉安市',15),
(2,'吉林市',9),
(7,'吐鲁番市',31),
(11,'吕梁市',6),
(3,'吴忠市',30),
(16,'周口市',17),
(7,'呼伦贝尔市',32),
(1,'呼和浩特市',32),
(11,'和田市',31),
(11,'咸宁市',18),
(4,'咸阳市',27),
(10,'哈密市',31),
(1,'哈尔滨市',10),
(2,'唐山市',5),
(14,'商丘市',17),
(10,'商洛市',27),
(9,'喀什市',31),
(17,'嘉义县',7),
(7,'嘉义市',7),
(4,'嘉兴市',12),
(5,'嘉峪关市',21),
(3,'四平市',9),
(4,'固原市',30),
(5,'图木舒克市',31),
(3,'基隆市',7),
(20,'塔城市',31),
(6,'大 庆 市',10),
(13,'大兴安岭地区',10),
(2,'大同市',6),
(13,'大理白族自治州',25),
(2,'大连市',8),
(4,'天水市',21),
(1,'天津市',2),
(14,'天门市',18),
(1,'太原市',6),
(19,'奎屯市',31),
(10,'威海市',16),
(13,'娄底市',19),
(9,'孝感市',18),
(9,'宁德市',14),
(2,'宁波市',12),
(8,'安庆市',13),
(9,'安康市',27),
(5,'安阳市',17),
(4,'安顺市',23),
(10,'定安县',24),
(11,'定西市',21),
(9,'宜兰县',7),
(13,'宜宾市',22),
(5,'宜昌市',18),
(9,'宜春市',15),
(3,'宝鸡市',27),
(17,'宣城市',13),
(12,'宿州市',13),
(13,'宿迁市',11),
(20,'屏东县',7),
(11,'屯昌县',24),
(4,'山南地区',29),
(6,'岳阳市',19),
(14,'崇左市',28),
(13,'巢湖市',13),
(17,'巴中市',22),
(8,'巴彦淖尔市',32),
(4,'常州市',11),
(7,'常德市',19),
(8,'平凉市',21),
(4,'平顶山市',17),
(7,'广元市',22),
(14,'广安市',22),
(1,'广州市',20),
(10,'庆阳市',21),
(13,'库尔勒市',31),
(10,'廊坊市',5),
(6,'延安市',27),
(9,'延边朝鲜族自治州',9),
(2,'开封市',17),
(7,'张家口市',5),
(8,'张家界市',19),
(7,'张掖市',21),
(14,'彰化县',7),
(3,'徐州市',11),
(14,'德宏傣族景颇族自治州',25),
(14,'德州市',16),
(5,'德阳市',22),
(9,'忻州市',6),
(12,'怀化市',19),
(15,'怒江傈傈族自治州',25),
(7,'思茅市',25),
(17,'恩施土家族苗族自治州',18),
(11,'惠州市',20),
(1,'成都市',22),
(10,'扬州市',11),
(8,'承德市',5),
(10,'抚州市',15),
(4,'抚顺市',8),
(1,'拉萨市',29),
(20,'揭阳市',20),
(3,'攀枝花市',22),
(9,'文山壮族苗族自治州',25),
(6,'文昌市',24),
(7,'新乡市',17),
(5,'新余市',15),
(11,'新竹县',7),
(6,'新竹市',7),
(2,'无锡市',11),
(5,'日喀则地区',29),
(11,'日照市',16),
(1,'昆明市',25),
(14,'昌吉市　',31),
(14,'昌江黎族自治县',24),
(3,'昌都地区',29),
(5,'昭通市',25),
(7,'晋中市',6),
(5,'晋城市',6),
(2,'景德镇市',15),
(2,'曲靖市',25),
(6,'朔州市',6),
(13,'朝阳市',8),
(5,'本溪市',8),
(13,'来宾市',28),
(1,'杭州市',12),
(7,'松原市',9),
(7,'林芝地区',29),
(6,'果洛藏族自治州',26),
(4,'枣庄市',16),
(2,'柳州市',28),
(2,'株洲市',19),
(3,'桂林市',28),
(10,'桃园县',7),
(12,'梅州市',20),
(4,'梧州市',28),
(12,'楚雄彝族自治州',25),
(8,'榆林市',27),
(6,'武威市',21),
(1,'武汉市',18),
(6,'毕节地区',23),
(11,'永州市',19),
(7,'汉中市',27),
(4,'汕头市',20),
(13,'汕尾市',20),
(7,'江门市',20),
(16,'池州市',13),
(1,'沈阳市',8),
(9,'沧州市',5),
(12,'河池市',28),
(14,'河源市',20),
(5,'泉州市',14),
(9,'泰安市',16),
(12,'泰州市',11),
(4,'泸州市',22),
(3,'洛阳市',17),
(1,'济南市',16),
(8,'济宁市',16),
(18,'济源市',17),
(2,'海东地区',26),
(3,'海北藏族自治州',26),
(5,'海南藏族自治州',26),
(1,'海口市',24),
(8,'海西蒙古族藏族自治州',26),
(3,'淄博市',16),
(6,'淮北市',13),
(4,'淮南市',13),
(8,'淮安市',11),
(2,'深圳市',20),
(16,'清远市',20),
(3,'温州市',12),
(5,'渭南市',27),
(5,'湖州市',12),
(3,'湘潭市',19),
(14,'湘西土家族苗族自治州',19),
(8,'湛江市',20),
(10,'滁州市',13),
(16,'滨州市',16),
(11,'漯河市',17),
(6,'漳州市',14),
(7,'潍坊市',16),
(15,'潜江市',18),
(19,'潮州市',20),
(9,'澄迈县',24),
(21,'澎湖县',7),
(1,'澳门特别行政区',33),
(9,'濮阳市',17),
(6,'烟台市',16),
(8,'焦作市',17),
(8,'牡丹江市',10),
(9,'玉林市',28),
(7,'玉树藏族自治州',26),
(3,'玉溪市',25),
(3,'珠海市',20),
(18,'琼中黎族苗族自治县',24),
(4,'琼海市',24),
(14,'甘南藏族自治州',21),
(20,'甘孜藏族自治州',22),
(8,'白城市',9),
(6,'白山市',9),
(13,'白沙黎族自治县',24),
(3,'白银市',21),
(10,'百色市',28),
(9,'益阳市',19),
(9,'盐城市',11),
(11,'盘锦市',8),
(12,'眉山市',22),
(2,'石嘴山市',30),
(1,'石家庄市',5),
(3,'石河子市　',31),
(16,'神农架林区',18),
(1,'福州市',14),
(3,'秦皇岛市',5),
(16,'米泉市',31),
(10,'红河哈尼族彝族自治州',25),
(6,'绍兴市',12),
(12,'绥 化 市',10),
(6,'绵阳市',22),
(15,'聊城市',16),
(10,'肇庆市',20),
(2,'自贡市',22),
(9,'舟山市',12),
(2,'芜湖市',13),
(23,'花莲县',7),
(5,'苏州市',11),
(12,'苗栗县',7),
(9,'茂名市',20),
(4,'荆州市',18),
(8,'荆门市',18),
(3,'莆田市',14),
(12,'莱芜市',16),
(17,'菏泽市',16),
(3,'萍乡市',15),
(8,'营口市',8),
(14,'葫芦岛市',8),
(3,'蚌埠市',13),
(11,'衡水市',5),
(4,'衡阳市',19),
(8,'衢州市',12),
(6,'襄樊市',18),
(11,'西双版纳傣族自治州',25),
(1,'西宁市',26),
(1,'西安市',27),
(10,'许昌市',17),
(8,'贵港市',28),
(1,'贵阳市',23),
(11,'贺州市',28),
(18,'资阳市',22),
(7,'赣州市',15),
(4,'赤峰市',32),
(4,'辽源市',9),
(10,'辽阳市',8),
(15,'达州市',22),
(8,'运城市',6),
(7,'连云港市',11),
(16,'迪庆藏族自治州',25),
(5,'通化市',9),
(5,'通辽市',32),
(8,'遂宁市',22),
(3,'遵义市',23),
(5,'邢台市',5),
(2,'那曲地区',29),
(4,'邯郸市',5),
(5,'邵阳市',19),
(1,'郑州市',17),
(10,'郴州市',19),
(6,'鄂尔多斯市',32),
(7,'鄂州市',18),
(9,'酒泉市',21),
(1,'重庆市',4),
(7,'金华市',12),
(2,'金昌市',21),
(7,'钦州市',28),
(12,'铁岭市',8),
(5,'铜仁地区',23),
(2,'铜川市',27),
(7,'铜陵市',13),
(1,'银川市',30),
(10,'锡林郭勒盟',32),
(7,'锦州市',8),
(11,'镇江市',11),
(1,'长春市',9),
(1,'长沙市',19),
(4,'长治市',6),
(15,'阜康市',31),
(9,'阜新市',8),
(11,'阜阳市',13),
(6,'防城港市',28),
(15,'阳江市',20),
(3,'阳泉市',6),
(8,'阿克苏市',31),
(22,'阿勒泰市',31),
(12,'阿图什市',31),
(19,'阿坝藏族羌族自治州',22),
(12,'阿拉善盟',32),
(4,'阿拉尔市',31),
(6,'阿里地区',29),
(12,'陇南市',21),
(16,'陵水黎族自治县',24),
(12,'随州市',18),
(16,'雅安市',22),
(2,'青岛市',16),
(3,'鞍山市',8),
(5,'韶关市',20),
(1,'香港特别行政区',34),
(5,'马鞍山市',13),
(17,'驻马店市',17),
(19,'高雄县',7),
(2,'高雄市',7),
(5,'鸡 西 市',10),
(3,'鹤 岗 市',10),
(6,'鹤壁市',17),
(6,'鹰潭市',15),
(10,'黄冈市',18),
(4,'黄南藏族自治州',26),
(9,'黄山市',13),
(2,'黄石市',18),
(11,'黑 河 市',10),
(8,'黔东南苗族侗族自治州',23),
(9,'黔南布依族苗族自治州',23),
(7,'黔西南布依族苗族自治州',23),
(2,'齐齐哈尔市',10),
(8,'龙岩市',14);

/*Table structure for table `coll_list` */

DROP TABLE IF EXISTS `coll_list`;

CREATE TABLE `coll_list` (
  `id` int NOT NULL AUTO_INCREMENT,
  `post_id` int NOT NULL,
  `user_id` int NOT NULL,
  `iscom` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`,`user_id`),
  KEY `post_id` (`post_id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `coll_list_ibfk_1` FOREIGN KEY (`post_id`) REFERENCES `post_list` (`id`),
  CONSTRAINT `coll_list_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

/*Data for the table `coll_list` */

insert  into `coll_list`(`id`,`post_id`,`user_id`,`iscom`) values 
(1,1,1,0),
(2,2,1,0);

/*Table structure for table `comment_img` */

DROP TABLE IF EXISTS `comment_img`;

CREATE TABLE `comment_img` (
  `id` int NOT NULL,
  `post_id` int NOT NULL COMMENT '帖子id',
  `comment_id` int NOT NULL COMMENT '评论id',
  `path` varchar(120) DEFAULT NULL COMMENT '地址',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

/*Data for the table `comment_img` */

/*Table structure for table `comments_list` */

DROP TABLE IF EXISTS `comments_list`;

CREATE TABLE `comments_list` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '评论id',
  `post_id` int NOT NULL COMMENT '帖子id',
  `uers_id` int NOT NULL COMMENT '用户id',
  `conment` varchar(120) NOT NULL COMMENT '评论内容',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `up` int DEFAULT '0' COMMENT '点赞数',
  PRIMARY KEY (`id`),
  KEY `post_id` (`post_id`),
  KEY `uers_id` (`uers_id`),
  CONSTRAINT `comments_list_ibfk_1` FOREIGN KEY (`post_id`) REFERENCES `post_list` (`id`),
  CONSTRAINT `comments_list_ibfk_2` FOREIGN KEY (`uers_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

/*Data for the table `comments_list` */

insert  into `comments_list`(`id`,`post_id`,`uers_id`,`conment`,`create_time`,`up`) values 
(1,1,1,'测试测试','2023-06-20 21:25:17',1),
(2,2,1,'测试测试','2023-06-20 21:25:38',0),
(3,19,2,'哒哒哒','2023-06-25 02:10:15',1),
(4,19,1,'1111','2023-06-25 02:27:31',0),
(5,19,1,'1231','2023-06-25 02:52:35',0),
(6,6,1,'1111','2023-06-25 02:53:43',0),
(7,21,1,'33','2023-06-25 02:54:16',0),
(8,14,1,'加油','2023-06-25 03:01:40',0),
(9,29,1,'111','2023-06-25 09:06:52',0),
(10,30,1,'11','2023-06-25 20:04:05',0),
(11,31,1,'11111','2023-06-27 14:44:52',0);

/*Table structure for table `menu` */

DROP TABLE IF EXISTS `menu`;

CREATE TABLE `menu` (
  `id` int NOT NULL AUTO_INCREMENT,
  `menu` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='用于存放导航栏';

/*Data for the table `menu` */

insert  into `menu`(`id`,`menu`) values 
(1,'帖子广场'),
(2,'图文'),
(3,'视频'),
(4,'同城');

/*Table structure for table `post_img` */

DROP TABLE IF EXISTS `post_img`;

CREATE TABLE `post_img` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '图片id',
  `post_id` int NOT NULL COMMENT '帖子id',
  `path` varchar(120) DEFAULT NULL COMMENT '图片地址',
  PRIMARY KEY (`id`),
  KEY `post_id` (`post_id`),
  CONSTRAINT `post_img_ibfk_1` FOREIGN KEY (`post_id`) REFERENCES `post_list` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=61 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

/*Data for the table `post_img` */

insert  into `post_img`(`id`,`post_id`,`path`) values 
(1,1,'assets/image/postImg/0001.jpg'),
(2,1,'assets/image/postImg/0002.jpg'),
(3,1,'assets/image/postImg/0003.jpg'),
(4,1,'assets/image/postImg/0004.jpg'),
(5,1,'assets/image/postImg/0005.jpg'),
(6,2,'assets/image/postImg/0011.jpg'),
(7,2,'assets/image/postImg/0012.jpg'),
(49,19,'assets/image/postImg/1687629134_0.jpg'),
(50,19,'assets/image/postImg/1687629134_1.jpg'),
(51,19,'assets/image/postImg/1687629134_2.jpg'),
(52,20,'assets/image/postImg/1687629327_0.jpg'),
(53,24,'assets/image/postImg/1687635492_0.jpg'),
(55,29,'assets/image/postImg/1687636108_0.jpg'),
(56,30,'assets/image/postImg/1687655238_0.jpg'),
(57,30,'assets/image/postImg/1687655238_1.jpg'),
(58,30,'assets/image/postImg/1687655238_2.jpg'),
(59,30,'assets/image/postImg/1687655238_3.jpg');

/*Table structure for table `post_list` */

DROP TABLE IF EXISTS `post_list`;

CREATE TABLE `post_list` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '帖子id',
  `title` varchar(50) NOT NULL COMMENT '帖子标题',
  `coment` varchar(999) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '帖子内容',
  `user_id` int NOT NULL COMMENT '用户id',
  `comments` int DEFAULT '0' COMMENT '评论数量',
  `up` int DEFAULT '0' COMMENT '点赞数量',
  `collection` int DEFAULT '0' COMMENT '收藏数量',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `Views` int DEFAULT '0' COMMENT '浏览量',
  `type_id` int DEFAULT '1' COMMENT '帖子类型1图文2视频',
  `topic_id` int DEFAULT NULL COMMENT '话题id',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `post_list_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

/*Data for the table `post_list` */

insert  into `post_list`(`id`,`title`,`coment`,`user_id`,`comments`,`up`,`collection`,`create_time`,`Views`,`type_id`,`topic_id`) values 
(1,'这是一个测试帖子','莫听穿林打叶声，何妨吟啸且徐行。\\n竹杖芒鞋轻胜马，谁怕？一蓑烟雨任平生。\\n料峭春风吹酒醒，微冷，山头斜照却相迎。',2,0,2,0,'2023-05-18 23:42:13',0,1,1),
(2,'我也是测试帖子','不用注意那穿林打叶的雨声，',1,0,2,0,'2023-05-18 23:43:05',0,1,2),
(4,'1233','123122313',1,0,2,0,'2023-06-21 00:29:58',0,1,NULL),
(5,'12','12',1,0,2,0,'2023-06-20 16:40:12',0,1,NULL),
(6,'测试测试','哈哈哈哈哈哈或或或或或或或或或或或或',1,0,0,0,'2023-06-20 16:46:23',0,1,NULL),
(14,'呜呜呜呜wwww','终于可以发帖子惹，我太辛酸惹~',1,0,0,0,'2023-06-25 01:41:20',0,1,NULL),
(19,'发个图文','发个图文',1,0,2,0,'2023-06-25 01:52:14',0,1,NULL),
(20,'测试','测试四',1,0,0,0,'2023-06-25 01:55:27',0,1,NULL),
(21,'11','123132',1,0,0,0,'2023-06-25 01:57:34',0,1,NULL),
(22,'44','44',1,0,0,0,'2023-06-25 01:59:08',0,1,NULL),
(23,'测试视频贴','测试测试',3,0,0,0,'2023-06-25 03:21:48',0,2,NULL),
(24,'测试','111',1,0,0,0,'2023-06-25 03:38:12',0,1,NULL),
(28,'测试视频','测试测试',1,0,0,0,'2023-06-25 03:48:02',0,2,NULL),
(29,'再测试一遍图文','再测试一遍图文',1,0,0,0,'2023-06-25 03:48:28',0,1,NULL),
(30,'1111','11111',1,0,0,0,'2023-06-25 09:07:18',0,1,NULL),
(31,'222','222',1,0,0,0,'2023-06-25 09:07:45',0,2,NULL),
(33,'1','1',2,0,0,0,'2023-07-23 17:10:02',0,1,NULL);

/*Table structure for table `post_video` */

DROP TABLE IF EXISTS `post_video`;

CREATE TABLE `post_video` (
  `id` int NOT NULL AUTO_INCREMENT,
  `post_id` int NOT NULL,
  `path` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `post_id` (`post_id`),
  CONSTRAINT `post_video_ibfk_1` FOREIGN KEY (`post_id`) REFERENCES `post_list` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

/*Data for the table `post_video` */

insert  into `post_video`(`id`,`post_id`,`path`) values 
(1,23,'assets/video/postvideo/video1.mp4'),
(4,28,'assets/video/postvideo/1687636082.mp4'),
(5,31,'assets/video/postvideo/1687655265.mp4');

/*Table structure for table `provincial` */

DROP TABLE IF EXISTS `provincial`;

CREATE TABLE `provincial` (
  `pid` int NOT NULL,
  `Provincial` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`pid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

/*Data for the table `provincial` */

insert  into `provincial`(`pid`,`Provincial`) values 
(1,'北京市'),
(2,'天津市'),
(3,'上海市'),
(4,'重庆市'),
(5,'河北省'),
(6,'山西省'),
(7,'台湾省'),
(8,'辽宁省'),
(9,'吉林省'),
(10,'黑龙江省'),
(11,'江苏省'),
(12,'浙江省'),
(13,'安徽省'),
(14,'福建省'),
(15,'江西省'),
(16,'山东省'),
(17,'河南省'),
(18,'湖北省'),
(19,'湖南省'),
(20,'广东省'),
(21,'甘肃省'),
(22,'四川省'),
(23,'贵州省'),
(24,'海南省'),
(25,'云南省'),
(26,'青海省'),
(27,'陕西省'),
(28,'广西壮族自治区'),
(29,'西藏自治区'),
(30,'宁夏回族自治区'),
(31,'新疆维吾尔自治区'),
(32,'内蒙古自治区'),
(33,'澳门特别行政区'),
(34,'香港特别行政区');

/*Table structure for table `relation_list` */

DROP TABLE IF EXISTS `relation_list`;

CREATE TABLE `relation_list` (
  `id` int NOT NULL AUTO_INCREMENT,
  `user_id` int NOT NULL COMMENT '用户id',
  `coll_user_id` int NOT NULL COMMENT '关注用户id',
  `state` tinyint(1) NOT NULL DEFAULT '1' COMMENT '关系状态',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `coll_user_id` (`coll_user_id`),
  CONSTRAINT `relation_list_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`),
  CONSTRAINT `relation_list_ibfk_2` FOREIGN KEY (`coll_user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

/*Data for the table `relation_list` */

insert  into `relation_list`(`id`,`user_id`,`coll_user_id`,`state`) values 
(1,1,2,1),
(2,1,3,0),
(3,1,19,1),
(4,2,1,1),
(5,3,1,1),
(6,3,2,1);

/*Table structure for table `toplist` */

DROP TABLE IF EXISTS `toplist`;

CREATE TABLE `toplist` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '话题id',
  `name` varchar(20) NOT NULL COMMENT '话题名',
  `avater` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT 'assets/image/topicImg/000.jpg' COMMENT '话题图片',
  `cont` varchar(100) DEFAULT '未添加更多描述' COMMENT '话题描述',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

/*Data for the table `toplist` */

insert  into `toplist`(`id`,`name`,`avater`,`cont`) values 
(1,'二手交易','assets/image/topicImg/0010.jpg','未添加更多描述'),
(2,'校园交友','assets/image/topicImg/0008.jpg','未添加更多描述'),
(3,'二次元日常','assets/image/topicImg/0003.jpg','未添加更多描述'),
(4,'学习俱乐部','assets/image/topicImg/0009.jpg','未添加更多描述'),
(5,'照片分享','assets/image/topicImg/0006.jpg','未添加更多描述'),
(6,'动漫','assets/image/topicImg/0007.jpg','未添加更多描述'),
(7,'校园树洞','assets/image/topicImg/0002.jpg','未添加更多描述'),
(8,'英语四六级','assets/image/topicImg/0001.jpg','未添加更多描述'),
(9,'一起来吐槽','assets/image/topicImg/0005.jpg','未添加更多描述'),
(10,'枫原万叶','assets/image/topicImg/话题1.jpg','未添加更多描述'),
(11,'提纳里','assets/image/topicImg/话题2.jpg','未添加更多描述'),
(12,'长野原宵宫','assets/image/topicImg/话题5.jpg','未添加更多描述'),
(13,'龙图','assets/image/topicImg/话题7.jpg','未添加更多描述'),
(14,'随手画','assets/image/topicImg/0004.jpg','未添加更多描述');

/*Table structure for table `user` */

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '用户ID',
  `uname` varchar(45) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '用户名',
  `paw` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '密码',
  `sex` varchar(45) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT '男' COMMENT '用户性别',
  `Phone` varchar(12) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '用户电话',
  `user_type` int NOT NULL DEFAULT '1' COMMENT '用户类型',
  `signature` varchar(60) DEFAULT NULL COMMENT '用户个性签名',
  `creat_time` date DEFAULT NULL COMMENT '注册时间',
  `avater` varchar(45) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT '/assets/image/avater/avater1.jpg' COMMENT '用户头像地址',
  `class` varchar(45) DEFAULT NULL COMMENT '班级',
  `school` varchar(45) DEFAULT NULL COMMENT '学校',
  `city_id` int DEFAULT NULL COMMENT '地区id',
  `school_id` int DEFAULT NULL,
  `pro_id` int DEFAULT NULL COMMENT '省份id',
  `message` int DEFAULT '0' COMMENT '消息数量',
  `collect` int DEFAULT '0' COMMENT '收藏帖子数',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uname_UNIQUE` (`uname`),
  KEY `pro_id` (`pro_id`),
  KEY `user_type` (`user_type`),
  CONSTRAINT `user_ibfk_1` FOREIGN KEY (`pro_id`) REFERENCES `provincial` (`pid`),
  CONSTRAINT `user_ibfk_2` FOREIGN KEY (`user_type`) REFERENCES `user_type` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

/*Data for the table `user` */

insert  into `user`(`id`,`uname`,`paw`,`sex`,`Phone`,`user_type`,`signature`,`creat_time`,`avater`,`class`,`school`,`city_id`,`school_id`,`pro_id`,`message`,`collect`) values 
(1,'芹菜粉','qq123124','男','18174462803',3,'时有好梦，亦怀爱意，向人间老去。','2023-05-18','/assets/image/avater/avater1.jpg','213546','张家界航院',1,2,15,0,0),
(2,'蓝二乘','qq123124','男','18322984285',2,'风窃然 浸润了我的心脏','2023-05-18','/assets/image/avater/avater2.jpg','213546','张家界航院',1,2,15,0,0),
(3,'test1','123','男','123456',1,'测试测试','2023-06-20','/assets/image/avater/0002.jpg',NULL,NULL,NULL,NULL,NULL,0,0),
(19,'张三','123','男','12345678901',1,'测试测试','2023-06-25','/assets/image/avater/0001.jpg',NULL,NULL,NULL,NULL,NULL,0,0);

/*Table structure for table `user_coll` */

DROP TABLE IF EXISTS `user_coll`;

CREATE TABLE `user_coll` (
  `id` int NOT NULL AUTO_INCREMENT,
  `user_id` int NOT NULL,
  `post_id` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `post_id` (`post_id`),
  CONSTRAINT `user_coll_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`),
  CONSTRAINT `user_coll_ibfk_2` FOREIGN KEY (`post_id`) REFERENCES `post_list` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

/*Data for the table `user_coll` */

insert  into `user_coll`(`id`,`user_id`,`post_id`) values 
(1,1,1),
(2,1,2),
(3,1,6);

/*Table structure for table `user_type` */

DROP TABLE IF EXISTS `user_type`;

CREATE TABLE `user_type` (
  `id` int NOT NULL COMMENT '用户类型id',
  `type_name` varchar(12) DEFAULT NULL COMMENT '用户类型名',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

/*Data for the table `user_type` */

insert  into `user_type`(`id`,`type_name`) values 
(1,'用户'),
(2,'管理员'),
(3,'超级管理员');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

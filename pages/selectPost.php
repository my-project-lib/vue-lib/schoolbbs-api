<?php
// header('Content-Type: application/json');

require_once "../includes/db.php";
// 查询所有帖子
$sql = "SELECT * FROM post_list ORDER BY create_time desc";
$data = [];
$result = $link->query($sql);
while($row=mysqli_fetch_array($result)){
    $user = mysqli_fetch_array($link->query("SELECT uname FROM user WHERE id =".number_format($row['user_id'])));
    $avater = mysqli_fetch_array($link->query("SELECT avater FROM user WHERE id =".number_format($row['user_id'])));
    $postNum =  mysqli_fetch_array($link->query("SELECT COUNT(*) FROM comments_list WHERE post_id=".$row['id']));
    $row['uname'] = $user[0];
    $row['collNum'] = $postNum[0];
    $row['avater'] = $avater[0];
    if($row['type_id']==2){
        $video = mysqli_fetch_array($link->query("SELECT path FROM post_video WHERE post_id =".number_format($row['id'])));
        $row['video'] = $video[0];
    }
    array_push($data,$row);
}
// var_dump($data);
echo json_encode($data);
$link->close();
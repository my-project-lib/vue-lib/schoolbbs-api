<!DOCTYPE html>
<html>
<head>
  <title>缩略图展示</title>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
</head>
<body>
  <div class="container">
    <div class="row">
      <div class="col-md-4">
        <a href="#" data-toggle="modal" data-target="#thumbnailModal">
          <img src="../assets/imges/lunbo/万叶1.jpg" class="img-thumbnail">
        </a>
      </div>
      <!-- 添加更多的缩略图 -->
    </div>
  </div>

  <!-- 模态框 -->
  <div id="thumbnailModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="thumbnailModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
          <h4 class="modal-title" id="thumbnailModalLabel">图片预览</h4>
        </div>
        <div class="modal-body">
          <img src="../assets/imges/lunbo/万叶1.jpg" class="img-responsive">
        </div>
      </div>
    </div>
  </div>

  <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</body>
</html>

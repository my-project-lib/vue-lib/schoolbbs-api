<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>帖子详情页</title>
  <link rel="stylesheet" href="../vendor/bootstarp/css/bootstrap.css">
  <style>
/* 
    .navbar {
      background-color: #337ab7;
      color: #fff;
    } */

    /* .post-title {
      font-size: 24px;
      color: #337ab7;
    } */

    .topic-list {
      background-color: #f7f7f7;
      padding: 10px;
      margin-bottom: 20px;
      font-size: 14px;
    }

    .post-content {
      margin-bottom: 20px;
      font-size: 16px;
    }

    .underline {
      border-bottom: 1px solid #ccc;
      margin: 20px 0;
    }

    .btn-like,
    .btn-favorite {
      font-size: 18px;
      color: #337ab7;
    }

    .user-info-card {
      background-color: #f7f7f7;
      padding: 20px;
      margin-bottom: 20px;
    }

    .user-avatar {
      width: 80px;
      height: 80px;
      border-radius: 50%;
    }

    .user-name {
      font-size: 18px;
      color: #337ab7;
      margin-bottom: 10px;
    }

    .btn-view-homepage {
      font-size: 16px;
      color: #fff;
      background-color: #337ab7;
      border-color: #337ab7;
      
    }

    .comment-card {
      background-color: #f7f7f7;
      padding: 20px;
      margin-bottom: 20px;
    }

    .comment-avatar {
      width: 60px;
      height: 60px;
      border-radius: 50%;
    }

    .comment-username {
      font-size: 16px;
      color: #337ab7;
      margin-bottom: 5px;
    }

    .comment-time {
      font-size: 12px;
      color: #999;
      margin-bottom: 10px;
    }

    .comment-content {
      margin-bottom: 10px;
    }

    .btn-like-comment,
    .btn-favorite-comment {
      font-size: 16px;
      color: #337ab7;
    }
    #post-s .repList{
        display: flex;
        background-color: #999;
    }
    #repList img{
        width: 50px;
        height: 50px;
    }
    #repList .panel-body .title-list{
        padding: 10px 0;
    }
    .title-list{
        display: flex;
        background-color: #b4b4b4;
        padding: 8px;
        margin: 5px 0;
    }
    .title-list>div{
        margin-right: 10px;
    }
    .title-list>div a{
        margin-left: 5px;
    }
    .btn-box{
        margin: 20px 0;
        text-align: right;
    }
    .rep-cont,.post-content{
        padding: 10px 0;
        font-size: 17px;
        letter-spacing:2px;
    }
    .user-card{
        display: flex;
        justify-content:space-between;
        text-align: center;
    }
    /*  */
    .input-file {
  position: relative;
}

.input-file__input {
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  opacity: 0;
  z-index: -1;
}

.input-file__label {
  display: inline-block;
  padding: 4px 8px;
  background-color: #337ab7;
  color: #fff;
  cursor: pointer;
}

.input-file__label:hover {
  background-color: #286090;
}

.input-file__label i {
  margin-right: 5px;
}

  </style>
</head>

<body>
<?php include '../templates/nav-bar.php'; ?>

  <!-- 内容布局 -->
  <div class="container" id="post-s">
    <div class="row">
      <div class="col-md-8">
        <div class="panel panel-primary">
          <div class="panel-body" style=" margin: 0 20px;">
            <h1>aaaaa</h1>
            <div class="title-list">
                <div>来自话题:<a href="">#qwq</a></div>
                <div>阅读量：<span>q</span>100</div>
            </div>
            <div class="post-content">
              帖子内容...
            </div>
            <div class="post-url">
                <img src="../assets/imges/达达利亚1.jpg" alt="">
            </div>
            <hr class="underline">
            <div>
              <button class="btn btn-like"><i class="glyphicon glyphicon-thumbs-up"></i> 赞</button>
              <button class="btn btn-favorite"><i class="glyphicon glyphicon-star"></i> 收藏</button>
            </div>
          </div>
        </div>
        <!-- 发表评论按钮 -->
        <div class="btn-box">
            <!-- <div class="row">
                <div class="col-md-9"></div>
                <div class="col-md-3"></div>
            </div> -->
            <button  class="btn btn-primary" data-toggle="modal" data-target="#commentModal">发表评论</button>
        </div>
        <!-- 个人资料 -->
        <div class="panel panel-primary" id="repList">
          <div class="panel-heading">
            <h3 class="panel-title">评论</h3>
          </div>
          <div class="panel-body">
            <div class="comment-card">
                <div class="media">
                    <div class="media-left">
                      <a href="#">
                        <img class="media-object" src="../assets/imges/达达利亚1.jpg" alt="...">
                      </a>
                    </div>
                    <div class="media-body">
                      <h4 class="media-heading">Media heading</h4>
                      ...
                    </div>
                  </div>
                  <div class="rep-cont">
                    哒哒哒
                  </div>
                <button class="btn btn-like-comment" ><a href="repup.php"><i class="glyphicon glyphicon-thumbs-up"></i> 赞</a></button>
                <button class="btn btn-favorite-comment"><i class="glyphicon glyphicon-star"></i> 收藏</button>
            </div>

          </div>
        </div>
      </div>

      <div class="col-md-4">
        <div class="panel panel-primary user-info-card">
          <div class="panel-body user-card">
            <img class="user-avatar" src="../assets/imges/达达利亚1.jpg" style="width: 120px;height: 120px;margin-top: 20px;border: #337ab7 2px dashed;" alt="用户头像">
            <div>
            <h3 class="user-name">用户名</h3>
            <p>发帖数量: 10</p>
            <p>评论数量: 20</p>
            <button class="btn btn-view-homepage">查看主页</button>
        </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- 模态框 -->
  <div class="modal fade" id="commentModal" tabindex="-1" role="dialog" aria-labelledby="commentModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title" id="commentModalLabel">发表评论</h4>
        </div>
        <div class="modal-body">
          <form>
            <div class="form-group">
              <label for="comment">评论内容</label>
              <textarea class="form-control" id="comment" rows="3"></textarea>

                <!-- <input type="file" class="form-control" id="img"> -->
                <div class="input-file">
                    <input type="file" id="file-input" class="input-file__input">
                    <label for="file-input" class="input-file__label">
                      <i class="glyphicon glyphicon-cloud-upload"></i> 选择文件
                    </label>
                  </div>
          </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
          <button type="button" class="btn btn-primary">发表</button>
        </div>
      </div>
    </div>
  </div>
</body>
<script src="../assets/js/jquery-3.6.0.js"></script>
<script src="../vendor/bootstarp/js/bootstrap.js"></script>
<script>
    // emjio输入
    
</script>
</html>

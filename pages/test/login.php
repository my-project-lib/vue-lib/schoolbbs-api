<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title>Login Page</title>
    <link rel="stylesheet" href="./login.css">
    <style>
      body {
        height: 100%;
        margin: 0;
        padding: 0;
        background-color: rgba(52, 203, 254, 0.2);
      }
      
      canvas {
        position: fixed;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
        z-index: -1;
      }
      #form{
        margin-top: 150px;
      }
      
    </style>
  </head>
  
  <body>
    <?php
        // 启动session
          session_start();

          // 检查session中是否有错误信息
          if (isset($_SESSION['errorMsg'])) {
              // 获取错误信息并弹窗显示
              $errorMsg = $_SESSION['errorMsg'];
              echo "<script>alert('$errorMsg');</script>";

              // 清除session中的错误信息
              unset($_SESSION['errorMsg']);
          }
        ?>
    <canvas id="background"></canvas>
    <!--  -->
    <div class="container" id="form">
      <div class="form-box">
          <!-- 注册 -->
          <form  class="register-box hidden" action="sign.php" >
              <h1>register</h1>
              <input type="text" placeholder="用户名" name="uname">
              <input type="tel" placeholder="手机号" name = "phone">
              <input type="password" placeholder="密码" >
              <input type="password" placeholder="确认密码" name="paw">
              <button type="submit">注册</button>
          </form >
          <!-- 登录 -->
          <form  class="login-box" action="to_login.php" >
              <h1>login</h1>
              <input type="text" placeholder="账号" name="phone">
              <input type="password" placeholder="密码" name="paw">
              <button type="submit">登录</button>
          </form >
      </div>
      <div class="con-box left">
          <h2>欢迎来到<span>校园贴吧</span></h2>
          <p>快来一起<span>交友</span>吧</p>
          <img src="../assets//imges/达达利亚1.jpg" alt="">
          <p>已有账号</p>
          <button id="login">去登录</button>
      </div>
      <div class="con-box right">
          <h2>欢迎来到<span>校园贴吧</span></h2>
          <p>快来一起<span>交友</span>吧</p>
          <img src="../assets//imges/达达利亚2.jpg" alt="">
          <p>没有账号？</p>
          <button id="register">去注册</button>
      </div>
  </div>
    
    <script>
        // 表单操作
        // 要操作到的元素
        let login=document.getElementById('login');
        let register=document.getElementById('register');
        let form_box=document.getElementsByClassName('form-box')[0];
        let register_box=document.getElementsByClassName('register-box')[0];
        let login_box=document.getElementsByClassName('login-box')[0];
        // 去注册按钮点击事件
        register.addEventListener('click',()=>{
            form_box.style.transform='translateX(80%)';
            login_box.classList.add('hidden');
            register_box.classList.remove('hidden');
        })
        // 去登录按钮点击事件
        login.addEventListener('click',()=>{
            form_box.style.transform='translateX(0%)';
            register_box.classList.add('hidden');
            login_box.classList.remove('hidden');
        })


      // Set up the canvas and context
      var canvas = document.getElementById("background");
      var ctx = canvas.getContext("2d");

      // Set up the canvas size and ratio for retina displays
      var width = window.innerWidth;
      var height = window.innerHeight;
      var PIXEL_RATIO = (function () {
          var ctx = document.createElement("canvas").getContext("2d"),
              dpr = window.devicePixelRatio || 1,
              bsr = ctx.webkitBackingStorePixelRatio ||
                    ctx.mozBackingStorePixelRatio ||
                    ctx.msBackingStorePixelRatio ||
                    ctx.oBackingStorePixelRatio ||
                    ctx.backingStorePixelRatio || 1;

          return dpr / bsr;
      })();
      
      canvas.width = width * PIXEL_RATIO;
      canvas.height = height * PIXEL_RATIO;
      canvas.style.width = width + "px";
      canvas.style.height = height + "px";
      ctx.setTransform(PIXEL_RATIO, 0, 0, PIXEL_RATIO, 0, 0);

      // Create an array of circles to animate
      var circles = [];
      for (var i = 0; i < 10; i++) {
        var radius = Math.random() * 100 + 50;
        var x = Math.random() * width;
        var y = Math.random() * height;
        var speed = Math.random() * 3 + 2;
        var color = "rgba(" + Math.random() * 255 + "," + Math.random() * 255 + "," + Math.random() * 255 + ",0.5)";

        circles.push({
          x: x,
          y: y,
          radius: radius,
          speed: speed,
          color: color
        });
      }

      // Animate the circles on the canvas
      function animate() {
        // Clear the canvas
        ctx.clearRect(0, 0, width, height);

        // Draw each circle on the canvas
        circles.forEach(function(circle) {
          ctx.beginPath();
          ctx.arc(circle.x, circle.y, circle.radius, 0, Math.PI * 2);
          ctx.fillStyle = circle.color;
          ctx.fill();

          // Move the circle based on its speed
          circle.x += circle.speed;

          // If the circle goes off screen, reset it to a random position
          if (circle.x > width + circle.radius) {
            circle.x = -circle.radius;
          }
        });

        // Call the animate function again
        requestAnimationFrame(animate);
      }

      // Start the animation loop
      animate();
    </script>
  </body>
</html>

<?php
 session_start();
 require_once "../includes/db.php";
 //编写sql语句
  $sql="SELECT * FROM  post_list";
  $uid=$_SESSION['uid'];
  $sql2 = "SELECT * FROM  `user` WHERE id = $uid";
 // //发送语句
  $result=$link->query($sql);
  $user_l=$link->query($sql2);
  $user = mysqli_fetch_array($user_l);
//   echo $user['avater'];
  $rs=$link->query('SELECT * FROM `menu`');
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=<device-width>, initial-scale=1.0">
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.10.0/font/bootstrap-icons.css">
    <link href="../vendor/bootstarp/css/bootstrap.css" rel="stylesheet">
    <script src="../assets/js/jquery-3.6.0.js"></script>
    <script src="../vendor/bootstarp/js/bootstrap.js"></script>
    <style>

        #postlist .media img {
            width: 80px;
            height: 80px;
        }

        #postlist .media-body {
            padding-top: 20px;
        }
        #postlist .panel-body .title{
            font-size:20px;
            font-weight: 600;
        }
        #postlist .panel-body  .coment{
            font-size:18px;
            padding:10px 0;
        }
        .post-foot{
            font-size: 18px;
            height:20px;
        }
        .post-foot span{
            padding: 0 10px;
            position: relative;
            top:3px;
        }
        .isup{
            color:blue;
        }
    </style>
</head>

<body>
    <?php include '../templates/nav-bar.php'; ?>
    <div class="container" id="postlist">
    <?php 
           while($row2=mysqli_fetch_array($result)){
            $post_u=$link->query("SELECT * FROM  `user` WHERE id =".$row2['user_id']);
            $puser=mysqli_fetch_array($post_u);
        //     var_dump( $row2['coment']);
        //     echo nl2br($row2['coment']);
        //     echo nl2br("莫听穿林打叶声，何妨吟啸且徐行。\n竹杖芒鞋轻胜马，谁怕？一蓑烟雨任平生。\n料峭春风吹酒醒，微冷，山头斜照却相迎。");
             ?>
        <!-- 单个帖子 -->
        <div class="panel panel-default">
            <!-- 用户信息 -->
            <div class="panel-heading">
                <div class="media">
                    <div class="media-left media-middle">
                        <a href="#">
                            <img class="media-object"
                                src="<?php echo $puser['avater'] ?>"
                                alt="...">
                        </a>
                    </div>
                    <div class="media-body">
                        <h3 class="media-heading"><?php echo $puser['uname'] ?></h3>
                        <p><?php echo $row2['create_time'] ?></p>
                    </div>
                </div>
            </div>
            <div class="panel-body">
                <div class="title" >
                  <a href="">
                <?php echo $row2['title'] ?>
              </a>
                </div>
                <!-- 帖子信息 -->
                <div class="coment">
                <?php echo  $row2['coment']?>
                </div>
                <div class="row">
                    <div class="col-xs-6 col-md-3">
                      <a href="#" class="thumbnail">
                        <img src="https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fc-ssl.duitang.com%2Fuploads%2Fblog%2F202105%2F17%2F20210517174237_e9a69.thumb.1000_0.jpg&refer=http%3A%2F%2Fc-ssl.duitang.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=auto?sec=1686970275&t=dcf0b78b34be445972c0e7f7db7be628" alt="...">
                      </a>
                    </div>
                    ...
                  </div>
                  <div class="post-foot row">
                    <div class="col-xs-6 col-md-3">
                        点赞
                        <span class="glyphicon  glyphicon-thumbs-up" aria-hidden="true"  id="up"></span>
                        <?php echo  $row2['up']?>
                        
                    </div>
                    <div class="col-xs-6 col-md-3">
                        评论
                        <span class="glyphicon  glyphicon-comment" aria-hidden="true" id="comments"></span>
                        <?php echo  $row2['comments']?>
                    </div>
                  </div>
            </div>

        </div>
        <?php } ?>
    </div>
</body>
<script>
        $('#up').click(function(){
            if($("#up").hasClass("isup")){
                $("#up").removeClass("isup");
            }else{
                $("#up").addClass("isup");
            }
        });
</script>
</html>
<?php
 session_start();
 //导入页面  连接数据库
 require_once "../includes/db.php";
 //编写sql语句
  $sql="SELECT * FROM `post_list` ";
 // //发送语句
  $result=$link->query($sql);

?>
<!DOCTYPE html>
<html>
<head>
  <title>校园贴吧</title>
  <link rel="stylesheet" href="../vendor/bootstarp/css/bootstrap.css">
  
  <style>
    body {
      background-color: #f5f5f5; /* 浅灰色背景 */
    }
    .container {
      margin-top: 20px;
    }
    /* .carousel {
      width: 100%;
    } */
    .post-card {
      margin-bottom: 20px;
      border: 1px solid #b8daff; /* 浅蓝色边框 */
      background-color: #fff; /* 白色背景 */
    }
    .post-card .user-info {
      display: flex;
      align-items: center;
      margin-bottom: 10px;
      font-size:17px;
    }
    .post-card .user-info .post-time{
      font-size:13px;
    }
    .post-card .user-info .avatar {
      width: 50px;
      height: 50px;
      border-radius: 50%;
      margin-right: 10px;
    }
    .post-card .post-title{
      font-size:22px;
      padding:10px 0;
    }
    .post-card .post-title:hover{
      color:blue;
    }
    .post-card .follow-button {
      margin-left: auto;
    }
    .post-card .post-content {
      white-space: nowrap;
      overflow: hidden;
      text-overflow: ellipsis;
    }
    .stats-container {
      display: flex;
      justify-content: flex-end;
      margin-top: 10px;
    }
    .stats-container .stat {
      margin-left: 10px;
    }
    .user-card {
      margin-bottom: 20px;
      border: 1px solid #b8daff; /* 浅蓝色边框 */
      background-color: #fff; /* 白色背景 */
    }
    .user-card .avatar {
      width: 100px;
      height: 100px;
      border-radius: 50%;
      margin-bottom: 10px;
    }
    .image-row img{
      width:120px;
      height:120px;
    }
    .user-card>div{
      display: flex;
      justify-content: space-between;
      align-items: center;
    }
    .user-card>div>div{
      display: flex;
      flex-direction: column;
      justify-content: center;
      align-items: center;
    }
    .publish-buttons {
      display: flex;
      flex-direction: column; /* 纵向排列按钮 */
      align-items: center;
      margin-top: 20px;
    }
    .publish-buttons .btn {
      margin: 5px 0; /* 调整按钮间距 */
    }
    /* 添加面板样式 */
    .panel {
      border-radius: 0;
      border: none;
      box-shadow: none;
    }
    .panel-default {
      background-color: #fff; /* 白色背景 */
      border: 1px solid #b8daff; /* 浅蓝色边框 */
    }
    .panel-heading {
      background-color: #337ab7;
      color: #fff;
      padding: 10px;
    }
    .panel-body {
      padding: 15px;
    }
     /* 更新样式 */
  .carousel {
    height: 300px; /* 设置轮播图高度 */
    overflow: hidden; /* 超出部分隐藏 */
  }

  .carousel .item img {
    width: 100%; /* 图片宽度为100% */
    height: auto; /* 高度自适应 */
    object-fit: cover; /* 图片裁剪以填充容器 */
  }
  </style>
  
</head>
<body>
  
<?php include '../templates/nav-bar.php'; ?>
  <div class="container">
    <div class="row">
      <div class="col-md-8">
        <div id="carousel" class="carousel slide" data-ride="carousel">
          <div class="carousel-inner">
            <div class="item active" id="lunbo">
              <img src="../assets/imges/lunbo/万叶1.jpg" alt="Image 1">
            </div>
            <div class="item">
              <img src="../assets/imges/lunbo/万叶2.jpg" alt="Image 2">
            </div>
            <div class="item">
              <img src="../assets/imges/lunbo/万叶3.jpg" alt="Image 3">
            </div>
          </div>
          <a class="left carousel-control" href="#carousel" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left"></span>
          </a>
          <a class="right carousel-control" href="#carousel" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right"></span>
          </a>
        </div>
        <?php
            while( $row=mysqli_fetch_array($result)){
              $post_uid = $row['user_id'];
              $post_u=$link->query('SELECT *  FROM  user WHERE id='.$post_uid);
              $post_i=$link->query('SELECT *  FROM  post_img WHERE post_id='.$row['id']);
              $post_uers = mysqli_fetch_array($post_u);
              // $post_uers = mysqli_fetch_array($post_i);
            
         
        ?>
        <div class="panel panel-default post-card" style="margin-top:30px">
  <div class="panel-body" >
    <div class="user-info">
      <img class="avatar" src="../assets/imges/万叶1.jpg" alt="User 1" :size="60">
      <div>
        <div class="username"><?php echo  $post_uers[1];?></div>
      <div class="post-time"><?php echo  $row['create_time'];?></div>
      </div>
      
      <button class="btn btn-primary follow-button">关注</button>
    </div>
    <div class="post-title">
    <?php echo  $row['title'];?>
    </div>
    <div class="post-content" style="padding-bottom:10px">
    <?php echo  $row['coment'];?>
    </div>
    <div class="row image-row">
      <div class="col-md-8">
      <?php
            while( $imgp=mysqli_fetch_array($post_i)){
            ?>
        <img class="post-image" src="../assets/<?php echo $imgp['path']?>" alt="Image 1">
        <?php
            }
            ?>
      </div>
      <div class="col-md-4">
        <div class="icon-container">
          <span class="glyphicon glyphicon-chevron-right"></span>
        </div>
      </div>
    </div>
    <div class="stats-container">
      <div class="stat"><a href=""><i class="glyphicon glyphicon-thumbs-up"></i> 100</a></div>
      <div class="stat"><a href=""><i class="glyphicon glyphicon-heart"></i> 50</a></div>
      <div class="stat"><a href=""><i class="glyphicon glyphicon-eye-open"></i> 1000</a></div>
    </div>
  </div>
  </div>
  <?php } ?>
</div>
      
      <div class="col-md-4">
        <div class="panel panel-default user-card">
          <div class="panel-body">
            <img class="avatar" src="../assets/imges/万叶1.jpg" alt="User 3">
            <div>
              <h4 class="username">用户名3</h4>
            <div class="signature">签名</div>
            </div>
            
          </div>
        </div>
        
        <div class="panel panel-default">
          <div class="panel-body">
            <div class="publish-buttons">
              <button class="btn btn-primary">发布帖子</button>
              <button class="btn btn-primary">发布图文</button>
              <button class="btn btn-primary">发布视频</button>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</body>
</html>

<nav class="navbar " style="background-color: #02b3e4; height: 65px;">
    <!-- <div class="container-fluid" style="line-height: 65px;"> -->
        <!-- 导航栏标题和图标 -->
        <div class="navbar-header">
            <a class="navbar-brand" href="#" style="font-size: 19px;"><span class="glyphicon glyphicon-home"></span> Logo</a>
        </div>

        <!-- 导航栏内容 -->
        <div class="collapse navbar-collapse">
            <ul class="nav navbar-nav" style="font-size: 19px;color:black">
                <li><a href="#"><span class="glyphicon glyphicon-dashboard"></span> Dashboard</a></li>
                <li><a href="#"><span class="glyphicon glyphicon-list"></span> Listings</a></li>
                <li><a href="#"><span class="glyphicon glyphicon-stats"></span> Statistics</a></li>
            </ul>

            <!-- 搜索框 -->
            <form class="navbar-form navbar-left"  >
                <div class="form-group">
                    <input type="text" class="form-control" placeholder="Search">
                </div>
                <button type="submit" class="btn btn-default">Submit</button>
            </form>

            <!-- 用户头像和下拉菜单 -->
            <ul class="nav navbar-nav navbar-right" >
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" style="font-size: 19px;">
                        <span class="glyphicon glyphicon-user"></span> User <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu" >
                        <li><a  href="#">Profile</a></li>
                        <li><a href="#">Settings</a></li>
                        <li class="divider"></li>
                        <li><a href="#">Logout</a></li>
                    </ul>
                </li>
            </ul>
        <!-- </div> -->
    </div>
</nav>

